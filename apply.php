<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//print_r($_POST);
//echo "Request method is " .$_SERVER['REQUEST_METHOD']."<br/>";

$servername = '127.0.0.1';
$username = 'helenleave';
$password = 'helenleave';
$status = 'Pending';
$userID = $_SESSION['user_id'];
$message = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=helen_leave", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$curr_date = date("Y-m-d H:i:s");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!$_POST['s'] || !$_POST['e'] || !$_POST['leave_type'] || !$_POST['reason']) {
        $message = "Submit Failed. Please complete the form before submitting";

    }else if (($_POST['s']<$curr_date)||($_POST['e']<$curr_date)){
        $message = "Submit Failed. Invalid dates. Please select future dates";
        }
     else {
        $sql = "INSERT INTO leave_system (start_Date, end_Date, leave_type, reason, user_id, status) 
                VALUES ('" . $_POST['s'] . "','" . $_POST['e'] . "','" . $_POST['leave_type'] . "','" . $_POST['reason'] . "','" . $userID . "','" . $status . "');";

        $conn->exec($sql);
    }

}
?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Leave Application</title>

    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">

</head>

<body>

<div class="topnav">
    <a  href="index.php" >Home</a>
    <a  href="apply.php" onclick="return false" style="text-decoration:none">Apply</a>
    <a  href="logout.php">Logout</a>
</div>


    <form method="post">
        <el-date-picker
                name="se"
                v-model="startEndTime"
                type="datetimerange"
                start-placeholder="Start Date"
                end-placeholder="End Date"
                :default-time="['12:00:00']">
        </el-date-picker>
        <br><br>
        Reason:
        <textarea name="reason" rows="4" cols="50"></textarea>
        <br><br>
        Type:
        <select name="leave_type">
            <option value="Annual">Annual</option>
            <option value="Off-in-lieu">Off-in-lieu</option>
            <option value="Compassionate">Compassionate</option>
            <option value="Maternity">Maternity</option>
            <option value="Unpaid">Unpaid</option>
        </select>

        <br><br>
        <input type="submit" value="Submit">

        <?php if ($message) echo '<div>'.$message.'</div>'; ?>


    </form>
</div>

</body>

<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="//unpkg.com/element-ui/lib/umd/locale/en.js"></script>
<script>
  ELEMENT.locale(ELEMENT.lang.en)
  new Vue({
    el: '#app',
    data: function() {
      return {
        visible: false,
        startEndTime: ''
      }
    }
  })
</script>
</html>




