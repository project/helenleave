<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//print_r($_POST);
//echo "Request method is " .$_SERVER['REQUEST_METHOD']."<br/>";

$servername = '127.0.0.1';
$username = 'helenleave';
$password = 'helenleave';
$userID = $_SESSION['user_id'];
$status = 'Pending';
$approval_level = 'CEO';

try {
    $conn = new PDO("mysql:host=$servername;dbname=helen_leave", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
}
catch(PDOException $e) {
    //echo "Connection failed: " . $e->getMessage();
}

// Use UserID to obtain user type.
$sql = "SELECT U.type, U.name
        FROM users U WHERE U.user_ID=$userID";
$user = $conn->query($sql)->fetchObject();
$user_type = $user->type;
$name = $user->name;



//$conn->close();

?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Leave Application</title>

    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">

</head>

<body>

<div class="topnav">
    <a  href="index.php" onclick="return false" style="text-decoration:none">Home</a>
    <a  href="apply.php">Apply</a>
    <a  href="logout.php" name = "logout">Logout</a>
</div>

    <h1>HOME</h1>
    <h2>Welcome Back, <?php echo $name; ?></h2>

</body>

<?php

$sql = "SELECT LS.id, LS.start_Date, LS.end_Date, LS.user_id, LS.leave_type, LS.status, U.name
        FROM `leave_system` LS JOIN users U ON U.user_id = LS.user_id".";";
$result = $conn->query($sql)->fetchALL(PDO ::FETCH_OBJ);


// View Login User's Leave Details.
echo "<div></div><table><tr><th>Name</th><th>Start Date</th><th>End Date</th><th>Leave Type</th><th>Status</th><th></th></tr>";
// output data of each row
foreach($result as $row) {

    if ($row->user_id == $userID) {
        echo "<tr>
        <td>" . $row->name . "</td>
        <td>" . $row->start_Date . "</td>
        <td>" . $row->end_Date . "</td>
        <td>" . $row->leave_type . "</td>
        <td>" . $row->status . "</td>
        <td><a href='view.php?id=" . $row->id ."'>View</a></td>
        </tr>";
    }
}
echo "</table></div>";


// View Other User's Leave Details. (Management Level Only)
if ($user_type==$approval_level){
echo "<div></div><table><tr><th>Name</th><th>Start Date</th><th>End Date</th><th>Leave Type</th><th>Status</th><th></th></tr>";
// output data of each row
}

foreach($result as $row) {

    if (($row->user_id != $userID)&&($user_type==$approval_level)) {

        echo "<tr>
        <td>" . $row->name . "</td>
        <td>" . $row->start_Date . "</td>
        <td>" . $row->end_Date . "</td>
        <td>" . $row->leave_type . "</td>
        <td>" . $row->status . "</td>
        <td><a href='view.php?id=" . $row->id ."'>View</a></td>
        </tr>";
    }
}
echo "</table></div>";

?>





<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="//unpkg.com/element-ui/lib/umd/locale/en.js"></script>
<script>
  ELEMENT.locale(ELEMENT.lang.en)
  new Vue({
    el: '#app',
    data: function() {
      return {
        visible: false,
      }
    }
  })
</script>
</html>
