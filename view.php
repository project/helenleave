<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//print_r($_POST);
//echo "Request method is " .$_SERVER['REQUEST_METHOD']."<br/>";

$servername = '127.0.0.1';
$username = 'helenleave';
$password = 'helenleave';
$userID = $_SESSION['user_id'];
$approval_level = 'CEO';
$leaveID = $_GET['id'];
$message = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=helen_leave", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
}
catch(PDOException $e) {
    //echo "Connection failed: " . $e->getMessage();
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //something posted

    if (isset($_POST["btnapprove"])) {
        $message = 'Leave is approved';

        //update status in SQL Query

        $sql = "UPDATE leave_system SET status = 'Approved' WHERE id=$leaveID";
        $result = $conn->query($sql);

    } else {
        $message = 'Leave is rejected';
        //update status in SQL Query

        $sql = "UPDATE leave_system SET status = 'Rejected' WHERE id=$leaveID";
        $result = $conn->query($sql);

    }
}

// Use UserID to obtain user type.
$sql = "SELECT U.type, U.name
        FROM users U WHERE U.user_ID=$userID";
$user = $conn->query($sql)->fetchObject();
$user_type = $user->type;

// Check leave status.
$sql = "SELECT LS.id, LS.start_Date, LS.end_Date, LS.user_id, LS.leave_type, LS.status, LS.reason, U.type, U.name 
        FROM `leave_system` LS JOIN users U ON U.user_id = LS.user_id WHERE LS.id =".$leaveID.";";
//echo $sql;

$applicant = $conn->query($sql)->fetchObject();
$applicant_type = $applicant->type;



?><!DOCTYPE html>
<html>
<head>
    <title>View Application</title>

</head>

<body>

<div class="topnav">
    <a  href="index.php" >Home</a>
    <a  href="apply.php" >Apply</a>
    <a  href="logout.php">Logout</a>
</div>

<h1>Leave Application ID: <?php echo $leaveID; ?></h1>


<?php if ($message) echo '<div>'.$message.'</div>'; ?>

<?php echo "Name: ".$applicant->name; ?><br>
<?php echo "Start Date: ".$applicant->start_Date; ?><br>
<?php echo "End Date: ".$applicant->end_Date; ?><br>
<?php echo "Leave Type: ".$applicant->leave_type; ?><br>
<?php echo "Reason: ".$applicant->reason; ?><br><br>
<?php echo "Status: ".$applicant->status; ?><br>

<form method="post">

    <?php
    if (($applicant->status == 'Approved')&&($user_type==$approval_level)){
        ?>
        <input type="submit" id = "btnreject" name="btnreject" value="Reject" />
        <?php
    } else if (($applicant->status == 'Pending')&&($user_type==$approval_level)){
        ?>
        <input type="submit" id = "btnapprove" name="btnapprove" value="Approve" />
        <input type="submit" id = "btnreject" name="btnreject" value="Reject" />
        <?php
    } else if ($user_type==$approval_level){
        ?>
        <input type="submit" id = "btnapprove" name="btnapprove" value="Approve" />
      <?php
    }
    ?>


</form>

</body>
</html>

